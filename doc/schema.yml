$schema: "https://json-schema.org/draft/2020-12/schema"
title: Partial Fix Data Set Schema
description: Schema for describing a task definition of the partial fix data set. The data set consists of manually curated partial fixes. Every partial fix is stored in a single directory that contains a number of zip files and a YML file. The ZIP files are snapshots of the projects before, during, and after fixing the issue. The YML file describes the partial fix. This schema describes how the YML file has to look like.  
type: object
properties:
  format_version:
    type: string
    description: Version of the schema.
  repository_url:
    type: string
    format: uri
    description: URL of the repository where this partial fix is from.
  sequence:
    type: object
    description: Information about the partial fixes.
    properties:
      base_version:
        type: object
        description: The repository at revision before first incomplete fix.
        properties:
          input_file: 
            type: string
            pattern: '[0-9a-f]{8}\.zip'
            description: A ZIP file containing the repository at revision `commit_sha1`.
          commit_sha1:
            type: string
            pattern: '[0-9a-f]+'
            description: SHA1 commit hash of the base version.
        required:
          - input_file
          - commit_sha1
      fix_attempt:
        type: array
        description: List of fix attempts.
        items:
          type: object
          description: Information about a fix attempt.
          properties:
            input_file: 
              type: string
              pattern: '[0-9a-f]{8}\.diff'
              description: Path to a DIFF file relative to the yml file containing the minimal diff between the last fix attempt (or the base version).
            commit_sha1:
              type: string
              pattern: '[0-9a-f]+'
              description: SHA1 commit hash of the fix attempt.
          required:
            - input_file
            - commit_sha1
      expected_fix:
        type: object
        description: Information about the expected fix.
        properties:
          input_file:
            type: string
            pattern: '[0-9a-f]{8}\.diff'
            description: Path to a DIFF file relative to the yml file containing the minimal diff between the last fix attempt and the expected fix.
          commit_sha1:
            type: string
            pattern: '[0-9a-f]+'
            description: SHA1 commit hash of the expected fix.
        required:
          - input_file
          - commit_sha1
    required:
      - base_version
      - fix_attempt
      - expected_fix
  classification:
    type: string
    enum:
      - partial fix
      - unknown
      - no partial fix
    description: Classification of the fix (partial fix, unknown, no partial fix).
  categories:
    type: array
    items:
      type: string
    description: List of bug-causing categories. The category helps to understand "what" should be fixed. Is it a build problem, a null pointer exception, a change of the control-flow or an arithmetic operation, fixing a test case or a CI, a problem that only occurs on certain hardwares, a wrong usage of the provided API (e.g., a function was called under wrong assumptions), race conditions, performance problems (e.g, a commit caused a slow-down in performance), or memory leaks?
  metadata:
    type: object
    description: Additional metadata about this task.
    properties:
      language:
        type: string
        description: Programming language of the original project and thus this partial fix.
      strategy:
        type: string
        description: The strategy used to mine this partial fix. Strategy 'reopen' means that the fix originates from an reopened issue. Strategy 'status' means that the issue originates from an issue where the CI failed before the last related commit fixed the CI. Strategy 'linux-convention' means that the partial fix originates from the linux kernel repository and was found by finding all commit chains with 3 or more commits.
        enum:
          - reopen
          - status
          - linux-convention  
      build_system:
        type: array
        items:
          type: string
        description: List of keywords indicating how to build the project.
      related_issue:
        type: [string, "null"]
        format: uri
        description: URL to an issue related to this partial fix.
    required:
      - language
required:
  - format_version
  - repository_url
  - sequence
  - classification
  - metadata

