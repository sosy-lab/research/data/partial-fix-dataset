#!/usr/bin/env python3

import argparse
import sys
from pathlib import Path

import jsonschema
import yaml


def _load_yaml(path: Path):
    with open(path, "r") as file:
        return yaml.safe_load(file)


def check_schema(schema: Path, task_def: Path):
    schema = _load_yaml(schema)
    yaml_data = _load_yaml(task_def)
    # we assume that all competition names match the regex `(SV-COMP|Test-Comp) 20[2-9][0-9]$`
    validator = jsonschema.validators.validator_for(schema)(schema)
    errors = sorted(validator.iter_errors(yaml_data), key=lambda e: e.path)
    for err in errors:
        print(f"{task_def}: {err.message}", file=sys.stderr)
    return len(errors)


def find_task_defs(root: Path):
    return root.glob("**/**/*.yml")


def parse_args():
    parser = argparse.ArgumentParser(
        description="Check all task definitions for schema compliance."
    )
    parser.add_argument("schema", type=Path, help="Path to the schema file.")
    parser.add_argument("task_defs", type=Path, help="Path to the task definitions.")
    return parser.parse_args()


def main():
    args = parse_args()
    task_defs = find_task_defs(args.task_defs)
    errors = 0
    for task_def in task_defs:
        errors += check_schema(args.schema, task_def)
    if errors == 0:
        print("All task definitions are valid.")
        sys.exit(0)
    else:
        print(f"Exit with failure after identifying {errors} errors.", file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    main()
