#!/usr/bin/env python3
import argparse
import multiprocessing
import os
import shutil
import subprocess
from pathlib import Path
from typing import List

import yaml


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "yaml_files", nargs="+", help="path to the yaml files to be initialized"
    )
    parser.add_argument(
        "--init-zips",
        action="store_true",
        help=(
            "Not only initialize the yaml files with the diffs,"
            " but also create the zips archives of the repository at that commit."
        ),
    )
    return parser.parse_args()


def execute_git_command(command: List[str], repository_path: Path):
    cwd = os.getcwd()
    os.chdir(repository_path)
    subprocess.check_call(command, stdout=subprocess.DEVNULL)
    os.chdir(cwd)


class GitUtils:
    # Do not clone the repository each time you need it
    def __init__(self, repository_path, repository_url):
        self.repository_path = repository_path
        self.repository_url = repository_url

    def clone_repository(self):
        execute_git_command(
            [
                "git",
                "clone",
                self.repository_url,
                str(self.repository_path.absolute()),
            ],
            self.repository_path.absolute().parent,
        )

    def archive_repository(self, commit_sha: str, output_path: Path):
        if not self.repository_path.exists():
            self.clone_repository()

        execute_git_command(
            [
                "git",
                "archive",
                "--format=zip",
                "-o",
                str(output_path.absolute()),
                commit_sha,
            ],
            self.repository_path,
        )

    def compute_diff(
        self,
        first_commit_sha: str,
        second_commit_sha: str,
        output_path: Path,
    ):
        if not self.repository_path.exists():
            self.clone_repository()

        execute_git_command(
            [
                "git",
                "diff",
                "-p",
                "--minimal",
                "--output",
                str(output_path.absolute()),
                first_commit_sha,
                second_commit_sha,
            ],
            self.repository_path,
        )


def get_diff(
    output_partial_fix, git_utils: GitUtils, commit_sha, last_commit, init_zips=False
):
    if not output_partial_fix.exists():
        git_utils.compute_diff(
            last_commit,
            commit_sha,
            output_partial_fix,
        )
    if init_zips:
        zip_output_file = output_partial_fix.with_suffix(".zip")
        if not zip_output_file.exists():
            git_utils.archive_repository(
                commit_sha,
                output_partial_fix.with_suffix(".zip"),
            )
    return


def get_repositories(yaml_file: Path, init_zips: bool = False):
    yaml_contents = yaml.safe_load(open(yaml_file))
    output_path = yaml_file.absolute().parent
    # Clone repository
    print(yaml_contents)
    repository_path = output_path / yaml_contents["repository_url"].split("/")[-1]

    commit_sequence = yaml_contents["sequence"]

    git_utils = GitUtils(repository_path, yaml_contents["repository_url"])

    # Base version
    commit_sha = commit_sequence["base_version"]["commit_sha1"]
    output_base_version = output_path / commit_sequence["base_version"]["input_file"]
    if not output_base_version.exists():
        git_utils.archive_repository(
            commit_sha,
            output_base_version,
        )

    # Compute diffs for all partial fixes
    last_commit = commit_sha
    for partial_fix in commit_sequence["fix_attempt"]:
        commit_sha = partial_fix["commit_sha1"]
        output_partial_fix = output_path / partial_fix["input_file"]
        get_diff(output_partial_fix, git_utils, commit_sha, last_commit, init_zips)
        last_commit = commit_sha

    # Compute diff for expected fix
    commit_sha = commit_sequence["expected_fix"]["commit_sha1"]
    output_final_fix = output_path / commit_sequence["expected_fix"]["input_file"]
    get_diff(output_final_fix, git_utils, commit_sha, last_commit, init_zips)

    # Delete repository
    shutil.rmtree(repository_path, ignore_errors=True)


def main():
    args = parse_args()
    pool = multiprocessing.Pool(max(1, os.cpu_count() - 2))
    pool.starmap(
        get_repositories,
        [(Path(yaml_file), args.init_zips) for yaml_file in args.yaml_files],
    )


if __name__ == "__main__":
    main()
